package amg.api.services;

import java.nio.charset.Charset;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.codec.Hex;
import org.springframework.security.crypto.password.Pbkdf2PasswordEncoder;
import org.springframework.stereotype.Service;

import amg.api.configs.CryptConfiguration;

@Service
public class PasswordService {
    @Autowired
    private CryptConfiguration cryptSetting;

    public CryptConfiguration getCryptSetting() {
        return cryptSetting;
    }

    public Pbkdf2PasswordEncoder pbkdf2PasswordEncoder;

    public void setCryptSetting(CryptConfiguration cryptSetting) {
        this.cryptSetting = cryptSetting;
    }

    public Pbkdf2PasswordEncoder getPbkdf2PasswordEncoder() {
        ensureEncoderNotNull();
        return this.pbkdf2PasswordEncoder;
    }

    public PasswordService() {
        // ensureEncoderNotNull();
    }

    private void ensureEncoderNotNull() {
        if (pbkdf2PasswordEncoder == null) {
            this.pbkdf2PasswordEncoder = new Pbkdf2PasswordEncoder(
                    new String(Hex.encode("salt".getBytes(Charset.forName("utf-8")))), cryptSetting.iterations,
                    cryptSetting.hashWidth);
        }
    }

    public String hash(String text) {
        return getPbkdf2PasswordEncoder().encode(text);
    }

    public Boolean verify(String text, String encryptedText) {
        return getPbkdf2PasswordEncoder().matches(text, encryptedText);
    }
}