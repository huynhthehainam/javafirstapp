package amg.api.permissions;

import java.io.Console;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.access.PermissionEvaluator;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import amg.dal.models.UserCache;
import amg.dal.services.BlogService;

@Component
public class CustomPermissionEvaluator implements PermissionEvaluator {

    @Override
    public boolean hasPermission(Authentication authentication, Object targetDomainObject, Object permission) {
        return false;
    }

    @Autowired
    private BlogService blogService;

    @Override
    public boolean hasPermission(Authentication authentication, Serializable targetId, String targetType,
            Object permission) {
        if ((authentication == null) || (targetType == null) || !(permission instanceof String)) {
            return false;

        }
        UserCache userCache = (UserCache) authentication.getPrincipal();
        if (targetType.equals("Blog")) {

            return true;
        }

        return false;
    }

}