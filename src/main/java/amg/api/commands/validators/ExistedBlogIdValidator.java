package amg.api.commands.validators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import amg.dal.services.BlogService;
import amg.dal.services.UserService;

@Component
public class ExistedBlogIdValidator implements ConstraintValidator<ExistedBlogId, Long> {

    @Autowired
    private BlogService blogService;


    @Override
    public void initialize(ExistedBlogId existedBlogIdConstraint) {
    }

    @Override
    public boolean isValid(Long value, ConstraintValidatorContext context) {
        return blogService.isExistedId(value);

    }

}