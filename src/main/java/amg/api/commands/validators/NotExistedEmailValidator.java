package amg.api.commands.validators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import amg.dal.services.BlogService;
import amg.dal.services.UserService;

@Component
public class NotExistedEmailValidator implements ConstraintValidator<NotExistedEmail, String> {

    @Autowired
    private UserService userService;;

    @Override
    public void initialize(NotExistedEmail notExistedEmailConstraint) {
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return !userService.isExistedEmail(value);
    }

}