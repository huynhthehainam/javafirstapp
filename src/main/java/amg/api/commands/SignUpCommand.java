package amg.api.commands;

import java.io.Serializable;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

public class SignUpCommand implements Serializable {
    @Email
    @NotNull
    public String email;

    @NotNull
    public String password;

    public String fullName;
}