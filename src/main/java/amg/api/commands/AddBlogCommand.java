package amg.api.commands;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

public class AddBlogCommand implements Serializable {
    @NotNull
    public String name;
    public String content;
}