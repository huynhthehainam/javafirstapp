package amg.api.commands;

import java.io.Serializable;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

public class LoginCommand implements Serializable {
    @NotNull
    @Email
    public String email;
    @NotNull
    public String password;
}