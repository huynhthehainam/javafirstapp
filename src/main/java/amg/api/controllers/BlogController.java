package amg.api.controllers;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import amg.api.commands.AddBlogCommand;
import amg.api.queries.FieldQueryBuilder;
import amg.api.queries.PageQueryBuilder;
import amg.api.queries.SearchCriteriaBuilder;
import amg.dal.models.Blog;
import amg.dal.services.BlogService;
import amg.infrastructure.responses.ActionResponse;

@RestController
@RequestMapping("blogs")
@Validated
public class BlogController extends BaseAuthorizedController {
    @Autowired
    private BlogService blogService;

    @PostMapping
    public ResponseEntity<ActionResponse> addBlog(@Valid @RequestBody AddBlogCommand command) {
        ActionResponse actionResponse = new ActionResponse();
        var currentUser = getCurrentUser();
        actionResponse.data = blogService.saveBlog(command.name, command.content, currentUser.email);
        return actionResponse.toResponseEntity();
    }

    @GetMapping("/")
    public ResponseEntity<ActionResponse> getBlogs(@RequestParam @Nullable String q,
            @RequestParam @Nullable String fields,
            @RequestParam @Nullable @Min(0) @Max(Integer.MAX_VALUE) Integer pageIndex,
            @RequestParam @Nullable @Min(1) @Max(Integer.MAX_VALUE) Integer pageSize) {
        ActionResponse actionResponse = new ActionResponse();
        SearchCriteriaBuilder<Blog> searchCriteriaBuilder = new SearchCriteriaBuilder<Blog>(Blog.class, q);
        PageQueryBuilder pageQueryBuilder = new PageQueryBuilder(pageSize, pageIndex);
        FieldQueryBuilder<Blog> fieldQueryBuilder = new FieldQueryBuilder<>(Blog.class, fields);
        var pageBlogs = blogService.getBlogs(searchCriteriaBuilder, pageQueryBuilder, fieldQueryBuilder,
                getCurrentUser().email);
        pageBlogs.setActionResponse(actionResponse);
        return actionResponse.toResponseEntity();
    }

    @GetMapping("/{id}")
    public ResponseEntity<ActionResponse> getBlog(@PathVariable Long id) {
        ActionResponse actionResponse = new ActionResponse();
        var blog = blogService.getBlog(id);
        if (blog != null) {
            actionResponse.data = blog;
        } else {
            actionResponse.addNotFoundError("id");
        }
        return actionResponse.toResponseEntity();
    }

}