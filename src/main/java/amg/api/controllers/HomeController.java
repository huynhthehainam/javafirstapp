package amg.api.controllers;

import java.io.Serializable;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import amg.infrastructure.responses.ActionResponse;


@RestController
@RequestMapping("/")
public class HomeController {
    @GetMapping
    public ResponseEntity<ActionResponse> index() {
        ActionResponse actionResponse = new ActionResponse();
        actionResponse.data = new Serializable() {
            public String CreatedBy = "Nam";
        };
        return actionResponse.toResponseEntity();
    }
}