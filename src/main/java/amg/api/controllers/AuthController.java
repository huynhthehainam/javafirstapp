package amg.api.controllers;

import java.io.Serializable;
import java.time.Duration;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import amg.api.commands.LoginCommand;
import amg.api.commands.SignUpCommand;
import amg.api.security.jwt.JwtUtils;
import amg.api.services.PasswordService;
import amg.dal.models.UserCache;
import amg.dal.services.UserService;
import amg.infrastructure.responses.ActionResponse;

@RestController
@RequestMapping("auth")
public class AuthController extends BaseController {

    @Autowired
    private PasswordService passwordService;
    @Autowired
    private UserService userService;
    @Autowired
    private JwtUtils jwtUtils;
    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

    @PostMapping("signup")
    public ResponseEntity<ActionResponse> signUp(@Valid @RequestBody SignUpCommand command) {
        ActionResponse actionResponse = new ActionResponse();
        if (!userService.isExistedEmail(command.email)) {
            userService.save(command.email, passwordService.hash(command.password), command.fullName, false);
            actionResponse.setCreatedMessage("user");
        } else {
            actionResponse.addConflictError("email");
        }
        return actionResponse.toResponseEntity();
    }

    @PostMapping("login")
    public ResponseEntity<ActionResponse> login(@Valid @RequestBody LoginCommand command) {
        ActionResponse actionResponse = new ActionResponse();
        var user = userService.getUser(command.email);
        if (user != null) {
            if (passwordService.verify(command.password, user.password)) {
                actionResponse.message = "logged in";
                actionResponse.data = new Serializable() {
                    public String accessToken = jwtUtils.generateJwtToken(user);
                };
                UserCache userCache = new UserCache(user);
                var redisKey = userService.buildRedisKey(user.email);
                redisTemplate.opsForValue().getAndSet(redisKey, userCache);
                redisTemplate.expire(redisKey, Duration.ofHours(5));
            }
        } else {
            actionResponse.addInvalidError("email or password");
        }
        return actionResponse.toResponseEntity();
    }
}