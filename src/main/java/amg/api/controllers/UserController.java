package amg.api.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import amg.dal.services.UserService;
import amg.infrastructure.responses.ActionResponse;

@RestController
@RequestMapping("user")
public class UserController extends BaseAuthorizedController {
    @Autowired
    private UserService userService;

    @GetMapping("profile")
    public ResponseEntity<ActionResponse> profile() {
        ActionResponse actionResponse = new ActionResponse();
        var currentUser = getCurrentUser();
        actionResponse.data = userService.getUser(currentUser.email);
        return actionResponse.toResponseEntity();
    }
}