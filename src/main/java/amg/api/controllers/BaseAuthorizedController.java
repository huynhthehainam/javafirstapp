package amg.api.controllers;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import amg.dal.models.UserCache;

public class BaseAuthorizedController extends BaseController {
    protected UserCache getCurrentUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return (UserCache) authentication.getPrincipal();
    }
}