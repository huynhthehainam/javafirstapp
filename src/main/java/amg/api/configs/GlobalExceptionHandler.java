package amg.api.configs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.validation.ConstraintViolationException;

import org.springframework.http.HttpStatus;
import org.springframework.messaging.handler.annotation.support.MethodArgumentNotValidException;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import amg.infrastructure.responses.ErrorMessage;

@ControllerAdvice
@Component
public class GlobalExceptionHandler {
    @ExceptionHandler
    @ResponseBody
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Map handle(MethodArgumentNotValidException exception) {
        List<ErrorMessage> errorMessages = null;
        if (exception.getBindingResult().getFieldErrors().size() > 0) {
            errorMessages = new ArrayList<ErrorMessage>();
        }
        for (var ex : exception.getBindingResult().getFieldErrors()) {
            List<String> fullFieldName = Arrays.asList(ex.getField().toString().split("\\."));
            if (fullFieldName.size() > 0) {
                var field = fullFieldName.get(fullFieldName.size() - 1);
                errorMessages.add(new ErrorMessage() {
                    {
                        setField(field);
                        setDefaultMessage(ex.getDefaultMessage());
                    }
                });
            }
        }
        return error(errorMessages);
    }

    @ExceptionHandler
    @ResponseBody
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Map handle(ConstraintViolationException exception) {
        List<ErrorMessage> errorMessages = null;
        if (exception.getConstraintViolations().size() > 0) {
            errorMessages = new ArrayList<ErrorMessage>();
        }
        for (var ex : exception.getConstraintViolations()) {
            List<String> fullFieldName = Arrays.asList(ex.getPropertyPath().toString().split("\\."));
            if (fullFieldName.size() > 0) {
                var fieldName = fullFieldName.get(fullFieldName.size() - 1);
                errorMessages.add(new ErrorMessage() {
                    {
                        setField(fieldName);
                        setDefaultMessage(ex.getMessage());
                    }
                });
            }
        }
        return error(errorMessages);
    }

    private Map error(List<ErrorMessage> messages) {

        return Collections.singletonMap("errors", messages);
    }
}