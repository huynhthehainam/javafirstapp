package amg.api.configs;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CryptConfiguration {
    @Value("${crypt.password.salt}")
    public String passwordSalt;
    @Value("${crypt.password.hashWidth}")
    public Integer hashWidth;
    @Value("${crypt.password.iterations}")
    public Integer iterations;
}