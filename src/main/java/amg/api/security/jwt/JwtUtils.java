package amg.api.security.jwt;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import amg.dal.models.User;
import amg.dal.models.UserCache;
import amg.dal.services.UserService;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;

@Component
public class JwtUtils {
    static final long ONE_MINUTE_IN_MILLIS = 60000;

    public JwtUtils() {
    }

    @Value("${jwt.secret}")
    private String jwtSecret;
    @Value("${jwt.expiredtime}")
    private int jwtExpirationMinutes;
    @Autowired
    private RedisTemplate<String, Object> redisTemplate;
    @Autowired
    private UserService userService;

    public String generateJwtToken(User user) {
        return Jwts.builder().setSubject(user.email).setIssuedAt(new Date())
                .setExpiration(new Date((new Date()).getTime() + jwtExpirationMinutes * ONE_MINUTE_IN_MILLIS))
                .signWith(SignatureAlgorithm.HS512, jwtSecret).compact();
    }

    public UserCache getUserCache(String token) {
        UserCache userCache = null;
        var email = Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token).getBody().getSubject();
        userCache = (UserCache) redisTemplate.opsForValue().get(userService.buildRedisKey(email));
        return userCache;
    }

    public boolean validateJwtToken(String authToken) {
        try {
            Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(authToken);
            return true;
        } catch (SignatureException e) {

        } catch (MalformedJwtException e) {
        } catch (ExpiredJwtException e) {
        } catch (UnsupportedJwtException e) {
        } catch (IllegalArgumentException e) {
        }

        return false;
    }

}