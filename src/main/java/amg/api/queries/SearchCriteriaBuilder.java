package amg.api.queries;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SearchCriteriaBuilder<T> {
    public List<SearchCriteria> searchCriteria = new ArrayList<SearchCriteria>();
    private List<String> fields = new ArrayList<String>();
    Pattern pattern = Pattern.compile("(\\w+?)(:|<|>)(\\w+?),");

    public void add(String key, String operation, Object value) {
        if (fields.contains(key)) {
            searchCriteria.add(new SearchCriteria(key, operation, value));
        }
    }

    public String toQuery(String tableSymbol) {
        var query = "";
        for (SearchCriteria search : searchCriteria) {
            query += search.toQuery(tableSymbol);
        }
        return query;
    }

    public SearchCriteriaBuilder(Class<T> doClass, String q) {

        for (Field field : doClass.getDeclaredFields()) {
            if (Modifier.isPublic(field.getModifiers())) {
                fields.add(field.getName());
            }
        }
        q = q + ",";
        Matcher matcher = pattern.matcher(q);
        while (matcher.find()) {
            add(matcher.group(1), matcher.group(2), matcher.group(3));
        }
    }
}