package amg.api.queries;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FieldQueryBuilder<T> {
    public List<String> selectedFields;

    public FieldQueryBuilder(Class<T> doClass, String fieldsString) {
        if (fieldsString != null) {
            selectedFields = new ArrayList<String>();
            List<String> fields = new ArrayList<String>();
            for (Field field : doClass.getDeclaredFields()) {
                if (Modifier.isPublic(field.getModifiers())) {
                    fields.add(field.getName());
                }
            }
            var splitFields = Arrays.asList(fieldsString.split("\\,"));
            for (String field : splitFields) {
                if (fields.contains(field)) {
                    selectedFields.add(field);
                }
            }
        }
    }

    public String buildSelectQuery(String tableName) {
        if (selectedFields == null)
            return "select * from public.\"" + tableName + "\" " + tableName + " ";
        var query = "select ";
        List<String> queryFields = new ArrayList<String>();
        for (String fieldString : selectedFields) {
            queryFields.add(" " + tableName + ".\"" + fieldString + "\" ");
        }
        query += String.join(",", queryFields);
        query += "from public.\"" + tableName + "\" " + tableName + " ";
        return query;
    }
}