package amg.api.queries;

import java.io.Serializable;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class PageQueryBuilder implements Serializable {

    public Integer pageSize;

    public Integer pageIndex;

    public Integer getPageSize() {
        return this.pageSize;
    }

    public void setPageSize(@NotNull @Min(1) @Max(Integer.MAX_VALUE) Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Integer getTotalPages(Integer totalRecords) {
        if (this.pageSize == null)
            return null;
        return (int) Math.ceil(totalRecords.doubleValue() / pageSize.doubleValue());
    }

    public Integer getPageIndex() {
        return this.pageIndex;
    }

    public void setPageIndex(@NotNull @Min(0) @Max(Integer.MAX_VALUE) Integer pageIndex) {
        this.pageIndex = pageIndex;
    }

    public String toQuery() {
        if (getPageIndex() != null && getPageSize() != null)
            return " limit " + getPageSize() + " offset " + getPageIndex();
        return "";
    }

    public PageQueryBuilder(@NotNull @Min(1) @Max(Integer.MAX_VALUE) Integer pageSize,
            @NotNull @Min(0) @Max(Integer.MAX_VALUE) Integer pageIndex) {
        if (pageSize != null && pageIndex != null) {
            this.pageSize = pageSize;
            this.pageIndex = pageIndex;
        }
    }
}