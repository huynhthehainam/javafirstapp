package amg.infrastructure.responses;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class ActionResponse {
    public HttpStatus statusCode = HttpStatus.OK;
    public Serializable data;
    public String message;
    public List<ErrorMessage> errors;
    public Integer pageIndex;
    public Integer totalPages;

    private void ensureErrorsNotNull() {
        if (this.errors == null)
            this.errors = new ArrayList<ErrorMessage>();
    }

    public ActionResponse() {

    }

    public void setCreatedMessage(String entityName) {
        this.message = entityName + "'s created";
        statusCode = HttpStatus.CREATED;
    }

    private void addError(String field, String message, HttpStatus status) {
        ensureErrorsNotNull();
        statusCode = status;
        ErrorMessage errorMessage = new ErrorMessage();
        errorMessage.setField(field);
        errorMessage.setDefaultMessage(message);
        this.errors.add(errorMessage);
    }

    public void addConflictError(String field) {
        addError(field, "exists", HttpStatus.CONFLICT);
    }

    public void addInvalidError(String field) {
        addError(field, "invalid", HttpStatus.BAD_REQUEST);
    }

    public void addNotFoundError(String field) {
        addError(field, "not found", HttpStatus.NOT_FOUND);
    }

    public ResponseEntity<ActionResponse> toResponseEntity() {
        return new ResponseEntity<ActionResponse>(this, statusCode);
    }
}