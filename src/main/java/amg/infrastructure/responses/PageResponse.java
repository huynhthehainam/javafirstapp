package amg.infrastructure.responses;

import java.io.Serializable;
import java.util.List;

public class PageResponse<T> implements Serializable {
    private List<T> data;
    private Integer pageIndex;
    private Integer totalPages;

    public List<T> getData() {
        return this.data;
    }

    public void setData(List<T> data) {
        this.data = data;
    }

    public PageResponse(List<T> data, Integer pageIndex, Integer totalPages) {
        this.data = data;
        this.pageIndex = pageIndex;
        this.totalPages = totalPages;
    }

    public Integer getPageIndex() {
        return this.pageIndex;
    }

    public void setPageIndex(Integer pageIndex) {
        this.pageIndex = pageIndex;
    }

    public Integer getTotalPages() {
        return this.totalPages;
    }

    public void setTotalPages(Integer totalPages) {
        this.totalPages = totalPages;
    }

    public void setActionResponse(ActionResponse actionResponse) {
        actionResponse.data = (Serializable) data;
        actionResponse.pageIndex = pageIndex;
        actionResponse.totalPages = totalPages;
    }
}
