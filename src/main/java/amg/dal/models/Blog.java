package amg.dal.models;

import java.io.Serializable;

public class Blog implements Serializable {
    public Long id;
    public String name;
    public String content;
    public String createdByEmail;

    public Long getId() {
        return this.id;
    }

    public String getCreatedByEmail() {
        return this.createdByEmail;
    }

    public void setCreatedByEmail(String createdByEmail) {
        this.createdByEmail = createdByEmail;
    }

   

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContent() {
        return this.content;
    }

    public void setContent(String content) {
        this.content = content;
    }

}