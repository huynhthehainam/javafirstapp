package amg.dal.models;

import java.io.Serializable;

public class UserCache implements Serializable {
    public String email;

    public UserCache() {
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public UserCache(User user) {
        this.email = user.email;
    }
}