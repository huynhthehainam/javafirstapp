package amg.dal.models;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnore;


public class User  implements Serializable {
    public String email;
    @JsonIgnore
    public String password;
    public String fullName;
    public Boolean isAdmin;

    public Boolean isIsAdmin() {
        return this.isAdmin;
    }

    public Boolean getIsAdmin() {
        return this.isAdmin;
    }

    public void setIsAdmin(Boolean isAdmin) {
        this.isAdmin = isAdmin;
    }


    public String getFullName() {
        return this.fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public User() {
    }

}