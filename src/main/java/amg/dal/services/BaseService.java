package amg.dal.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

public class BaseService {
    @Autowired
    protected JdbcTemplate jdbcTemplate;
}