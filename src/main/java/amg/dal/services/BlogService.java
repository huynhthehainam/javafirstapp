package amg.dal.services;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Service;

import amg.api.queries.FieldQueryBuilder;
import amg.api.queries.PageQueryBuilder;
import amg.api.queries.SearchCriteriaBuilder;
import amg.dal.models.Blog;
import amg.infrastructure.responses.PageResponse;

@Service
public class BlogService extends BaseService {
        public static final String tableName = "blogs";

        public Blog saveBlog(String name, String content, String createdUserEmail) {
                var sql = "insert into public.\"" + tableName + "\" "
                                + " (\"name\",\"content\",\"createdByEmail\") values (?,?,?) returning *";
                return jdbcTemplate
                                .query(sql, new Object[] { name, content, createdUserEmail },
                                                BeanPropertyRowMapper.newInstance(Blog.class))
                                .stream().findFirst().orElse(null);
        }

        public Blog getBlog(Long id) {
                var sql = "select * from " + tableName + " b where b.\"id\" = ?";
                return jdbcTemplate.query(sql, new Object[] { id }, BeanPropertyRowMapper.newInstance(Blog.class))
                                .stream().findFirst().orElse(null);
        }

        public PageResponse<Blog> getBlogs(SearchCriteriaBuilder<Blog> searchBuilder, PageQueryBuilder pageQueryBuilder,
                        FieldQueryBuilder<Blog> fieldQueryBuilder, String currentUserEmail) {
                var conditionSql = " where 1=1 ";
                conditionSql += currentUserEmail != null
                                ? " and " + tableName + ".\"createdByEmail\" = '" + currentUserEmail + "'"
                                : "";
                conditionSql += searchBuilder.toQuery(tableName);
                var selectCountSql = "select count(*) from public.\"" + tableName + "\" ";
                var sql = selectCountSql + conditionSql;
                Integer totalRecords = jdbcTemplate.query(sql, (rs, rowNum) -> rs.getInt("count")).stream().findFirst()
                                .orElse(null);

                var pageSql = pageQueryBuilder != null ? pageQueryBuilder.toQuery() : "";
                var selectSql = fieldQueryBuilder.buildSelectQuery(tableName);

                sql = selectSql + conditionSql + pageSql;
                var blogs = jdbcTemplate.query(sql, new Object[] {}, BeanPropertyRowMapper.newInstance(Blog.class));
                PageResponse<Blog> pageResponse = new PageResponse<>(blogs, pageQueryBuilder.getPageIndex(),
                                pageQueryBuilder.getTotalPages(totalRecords));
                return pageResponse;
        }

        public Boolean isExistedId(Long id) {
                var sql = "select exists (select * from " + tableName + " b where b.\"id\" = ?)";
                return jdbcTemplate.query(sql, new Object[] { id }, (rs, rowNum) -> rs.getBoolean("exists")).stream()
                                .findFirst().orElse(false);
        }
}