package amg.dal.services;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Service;

import amg.dal.models.User;

@Service
public class UserService extends BaseService {
    public static final String tableName = "public.\"users\" u";
    @Value("${usercache.prefix}")
    public String userCachePrefix;

    public User getUser(String email) {
        var sql = "select * from " + tableName + " where u.email = ? limit 1";
        return jdbcTemplate.query(sql, new Object[] { email }, BeanPropertyRowMapper.newInstance(User.class)).stream()
                .findFirst().orElse(null);
    }

    public User save(String email, String password, String fullName, Boolean isAdmin) {
        var sql = "insert into " + tableName
                + " (\"email\", \"password\", \"fullName\",\"isAdmin\") values (?,?,?,?) returning *";
        return jdbcTemplate.query(sql, new Object[] { email, password, fullName, isAdmin },
                BeanPropertyRowMapper.newInstance(User.class)).stream().findFirst().orElse(null);
    }

    public String buildRedisKey(String userEmail) {
        return userCachePrefix + ":" + userEmail;
    }

    public Boolean isExistedEmail(String email) {
        var sql = "select exists (select * from " + tableName + " where u.\"email\" = ?)";
        return jdbcTemplate.query(sql, new Object[] { email }, (rs, rowNum) -> rs.getBoolean("exists")).stream()
                .findFirst().orElse(false);
    }

}